from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.


def home(request):
    #return HttpResponse("My first Django Application!")

    name = request.POST.get("username")
    return render(request, "home.html", context={"name": name})

def myhome(request):
    return render(request, "myhome.html")
